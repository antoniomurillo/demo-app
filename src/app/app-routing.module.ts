import {NgModule} from '@angular/core';
import {Routes, RouterModule, PreloadAllModules} from '@angular/router';

import {DetailComponent} from './detail/detail.component';
import {HomeComponent} from './core/home/home.component';
import {AuthGuard} from './auth/auth-guard.service';

const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'detail', component: DetailComponent, canActivate: [AuthGuard]}
];

/**
 * Application routing module.
 */
@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule],
    providers: [
        AuthGuard
    ]
})
export class AppRoutingModule {

}
