import {NgModule} from '@angular/core';
import {Http} from '@angular/http';

import {TranslateLoader, TranslateModule} from '@ngx-translate/core';

import {SignInComponent} from './signin/signin.component';
import {AuthRoutingModule} from './auth-routing.module';
import {HttpLoaderFactory} from '../core/core.module';

import {SharedModule} from '../shared/shared.module';

/**
 * Authentication module.
 */
@NgModule({
    declarations: [
        SignInComponent
    ],
    imports: [
        SharedModule,
        AuthRoutingModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [Http]
            }
        })
    ]
})
export class AuthModule {
}
