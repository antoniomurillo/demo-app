import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {Router} from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';

import * as AuthActions from './auth.actions';

/**
 * Fail action.
 */
@Injectable()
export class AuthEffects {

    @Effect()
    authSignIn = this.actions$
        .ofType(AuthActions.TRY_SIGN_IN)
        .map((action: AuthActions.TrySignIn) => {
            return action.payload;
        })
        .switchMap((authData: { username: string, password: string }) => {
            if (authData.username === authData.password) {
                this.router.navigate(['/detail']);
                return [
                    {
                        type: AuthActions.SIGN_IN
                    },
                    {
                        type: AuthActions.SET_TOKEN,
                        payload: 'token'
                    }
                ];
            } else {
                return [
                    {
                        type: AuthActions.FAIL
                    }
                ];
            }
        });

    @Effect({dispatch: false})
    authLogout = this.actions$
        .ofType(AuthActions.LOGOUT)
        .do(() => {
            this.router.navigate(['/']);
        });

    constructor(private actions$: Actions, private router: Router) {
    }
}
