import * as AuthActions from './auth.actions';

/**
 * State interface.
 */
export interface State {
    token: string;
    authenticated: boolean;
    fail: boolean;
}

const initialState: State = {
    token: null,
    authenticated: false,
    fail: false
};

/**
 * Authentication reducer.
 * @param {State} state
 * @param {AuthActions} action
 * @returns {any} Next state
 */
export function authReducer(state = initialState, action: AuthActions.AuthActions) {
    switch (action.type) {
        case (AuthActions.SIGN_IN):
            return {
                ...state,
                authenticated: true,
                fail: false
            };
        case (AuthActions.LOGOUT):
            return {
                ...state,
                token: null,
                authenticated: false
            };
        case (AuthActions.SET_TOKEN):
            return {
                ...state,
                token: action.payload
            };
        case (AuthActions.FAIL):
            return {
                ...state,
                fail: true
            };
        default:
            return state;
    }
}
