import {Action} from '@ngrx/store';

export const TRY_SIGN_IN = 'TRY_SIGN_IN';
export const SIGN_IN = 'SIGN_IN';
export const LOGOUT = 'LOGOUT';
export const SET_TOKEN = 'SET_TOKEN';
export const FAIL = 'FAIL';

/**
 * TrySignIn action.
 */
export class TrySignIn implements Action {
    readonly type = TRY_SIGN_IN;

    constructor(public payload: { username: string, password: string }) {
    }
}

/**
 * SignIn action.
 */
export class SignIn implements Action {
    readonly type = SIGN_IN;
}

/**
 * Logout action.
 */
export class Logout implements Action {
    readonly type = LOGOUT;
}

/**
 * SetToken action.
 */
export class SetToken implements Action {
    readonly type = SET_TOKEN;

    constructor(public payload: string) {
    }
}

/**
 * Fail action.
 */
export class Fail implements Action {
    readonly type = FAIL;
}

/**
 * Available actions for authentication.
 */
export type AuthActions = SignIn | Logout | SetToken | TrySignIn | Fail;
