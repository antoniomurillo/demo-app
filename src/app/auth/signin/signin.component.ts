import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';

import {Store} from '@ngrx/store';
import {TranslateService} from '@ngx-translate/core';

import {Observable} from 'rxjs/Observable';

import * as fromApp from '../../store/app.reducers';
import * as AuthActions from '../store/auth.actions';

import * as fromAuth from '../store/auth.reducers';

/**
 * Sign in component.
 */
@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.css']
})
export class SignInComponent implements OnInit {
    authState: Observable<fromAuth.State>;

    constructor(private store: Store<fromApp.AppState>,
                public translate: TranslateService) {
    }

    ngOnInit() {
        this.authState = this.store.select('auth');
    }

    onSignIn(form: NgForm) {
        const username = form.value.username;
        const password = form.value.password;
        this.store.dispatch(new AuthActions.TrySignIn({username: username, password: password}));
    }

}
