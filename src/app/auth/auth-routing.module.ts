import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {SignInComponent} from './signin/signin.component';

const authRoutes: Routes = [
    {path: 'signin', component: SignInComponent}
];

/**
 * Routing module for Authentication.
 */
@NgModule({
    imports: [
        RouterModule.forChild(authRoutes)
    ],
    exports: [RouterModule]
})
export class AuthRoutingModule {
}
