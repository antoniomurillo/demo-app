import {Component} from '@angular/core';

/**
 * Application component.
 */
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'app';
    layoutCompact = true;
}
