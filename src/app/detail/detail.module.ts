import {NgModule} from '@angular/core';

import {TranslateModule} from '@ngx-translate/core';

import {DetailComponent} from './detail.component';
import {SharedModule} from '../shared/shared.module';


@NgModule({
    imports: [
        SharedModule,
        TranslateModule
    ],
    declarations: [
        DetailComponent
    ]
})
export class DetailModule {
}
