import { Component, OnInit } from '@angular/core';

import {TranslateService} from '@ngx-translate/core';

/**
 * Detail component.
 */
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  constructor(public translate: TranslateService) { }

  ngOnInit() {
  }

}
