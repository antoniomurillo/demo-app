import {forwardRef, Inject, Injectable} from '@angular/core';
import {Http} from '@angular/http';

import {TranslateLoader} from '@ngx-translate/core';

import 'rxjs/add/operator/map';

/**
 * Custom translation loader to allow change resource language paths
 */
@Injectable()
export class CustomTranslationLoader implements TranslateLoader {

    private http: Http;
    private prefix: string;
    private suffix: string;

    constructor(@Inject(forwardRef(() => Http)) http: Http, prefix?: string, suffix?: string) {
        if (prefix === void 0) {
            prefix = '/assets/i18n/';
        }
        if (suffix === void 0) {
            suffix = '.json';
        }
        this.http = http;
        this.prefix = prefix;
        this.suffix = suffix;
    }

    getTranslation(lang: string) {
        return this.http.get('.' + this.prefix + lang + this.suffix);
    }

}
