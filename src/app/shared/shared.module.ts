import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ConfirmDialogModule} from 'primeng/components/confirmdialog/confirmdialog';
import {
    AutoCompleteModule, ChartModule, DataListModule, DataTableModule, DialogModule, DropdownModule, EditorModule,
    FieldsetModule, FileUploadModule, GrowlModule, InputMaskModule, InputTextareaModule, InputTextModule,
    MultiSelectModule, PaginatorModule, PanelModule, SliderModule, SpinnerModule, StepsModule, TabViewModule
} from 'primeng/primeng';

import {TranslateModule} from '@ngx-translate/core';

export const primeModules = [
    AutoCompleteModule,
    ConfirmDialogModule,
    DialogModule,
    PanelModule,
    DataTableModule,
    DataListModule,
    MultiSelectModule,
    DropdownModule,
    FieldsetModule,
    InputMaskModule,
    InputTextModule,
    InputTextareaModule,
    FileUploadModule,
    GrowlModule,
    EditorModule,
    TabViewModule,
    SpinnerModule,
    StepsModule,
    SliderModule,
    ChartModule,
    PaginatorModule,
    TranslateModule
];

/**
 * Shared module.
 */
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        primeModules,
        TranslateModule.forChild()
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        primeModules,
        TranslateModule
    ]
})
export class SharedModule {
}
