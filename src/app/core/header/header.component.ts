import {Component, OnInit} from '@angular/core';

import {Store} from '@ngrx/store';
import {TranslateService} from '@ngx-translate/core';

import {Observable} from 'rxjs/Observable';

import * as fromApp from '../../store/app.reducers';
import * as fromAuth from '../../auth/store/auth.reducers';
import * as AuthActions from '../../auth/store/auth.actions';

/**
 * Header component.
 */
@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
    authState: Observable<fromAuth.State>;
    darkMenu = false;
    languages = ['en', 'fr', 'nl'];

    constructor(private store: Store<fromApp.AppState>, public translate: TranslateService) {
        translate.addLangs(this.languages);
        translate.setDefaultLang(this.languages[0]);
        const browserLang = localStorage.getItem('lang') ? localStorage.getItem('lang') : translate.getBrowserLang();
        translate.use(browserLang.match(new RegExp(this.languages.join('|'), 'gi')) ? browserLang : translate.getDefaultLang());
    }

    ngOnInit() {
        this.authState = this.store.select('auth');
    }

    onLogout() {
        this.store.dispatch(new AuthActions.Logout());
    }
}
