import {ActionReducerMap} from '@ngrx/store';

import * as fromAuth from '../auth/store/auth.reducers';

/**
 * Application state interface.
 */
export interface AppState {
    auth: fromAuth.State;
}

/**
 * Reducers available.
 */
export const reducers: ActionReducerMap<AppState> = {
    auth: fromAuth.authReducer
};
